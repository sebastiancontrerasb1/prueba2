package test.android.prueba2.fragmentos;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import test.android.prueba2.R;


public class DatosPersonalesFragment extends Fragment {


    private OnFragmentInteractionListener mListener;

    public DatosPersonalesFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.fragment_datos_personales, container, false);

        final EditText etNombre = (EditText) layout.findViewById(R.id.etNombre);
        final EditText etEdad = (EditText) layout.findViewById(R.id.etEdad);
        final Button btnSiguiente = (Button) layout.findViewById(R.id.btnSiguiente);

        btnSiguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Eliminar espacios en blancos y que el campo no este vacio
                String nombre = etNombre.getText().toString().trim();
                if (nombre.isEmpty()){

                    //Modificar si da error
                    etNombre.setError("Campo obligatorio");
                    etNombre.requestFocus();

                }else {
                    mListener.onFragmentInteraction("DatosPersonalesFragment", "BTN_SIGUIENTE");
                }
            }
        });
        return layout;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction("", "");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String nombreFragmento, String eventoFragmento);
    }
}
